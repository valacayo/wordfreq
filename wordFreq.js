//Original - https://gist.githubusercontent.com/lsauer/1221325/raw/d72149c94629cd2fcab7bf4ebc717c4f41cb6e78/gistfile1.js
//Alt. Idea - https://gist.githubusercontent.com/rocktronica/2625413/raw/54e6b8e85eb83fd764cce43194abd135b5c6b0f9/wordfrequency.js

var fs = require('fs');

var wordFreq = function(folderPath){
  var wDir = fs.readdirSync(folderPath);
  var wList = [];
  for (var i = 0; i < wDir.length; i++){
    if (wDir[i].match(/^[^\.].+\.html$/)){
      var wText = fs.readFileSync(folderPath + '/' + wDir[i], 'utf8');
      wList.push(wText);
    }
  }
  var wordcnt = function(){
    var hist = {};

    var textString = wList.join().replace(/[\r\n]/g, ' ').replace(/<head>.+?<\/head>/g,' ').replace(/<.+?>|\{.+?\}|&.+?;|-->/g, ' ').replace(/[.:;,!¡?]/g, '').replace(/\s{2,}/g, ' ');
    var words = textString.toLowerCase().split(/[\s\(\)]/).sort();

    var cnt = function(input,output){
      for(var i in input){
        if(input[i].length >1) {
          if (output[input[i]])  {
            output[input[i]]+=1;
          } else {
            output[input[i]]=1;
          }
        }
      }
    }
    cnt(words,hist);
    return hist;
};
  return wordcnt();
};

console.log(wordFreq('html'));